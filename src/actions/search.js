import Axios from 'axios';
import * as actionTypes from '../constants/actionTypes';

import {trackGenreUrl, trackQueryUrl} from '../constants/songConstants';

function    searchRequest(query) {
    return {
        type: actionTypes.SEARCH_QUERY,
        query: query.data
    }
}

export function searchQuery(query) {
    return (dispatch) => {
        Axios.get(trackQueryUrl + query + '&limit=100')
            .then((tracks) => {
                dispatch(searchRequest(tracks));
            })
            .catch((err) => {
                console.error(err);
            })
    }
}

export function searchGenre(genre) {
    return (dispatch) => {
        Axios.get(trackGenreUrl + genre + '&limit=100')
            .then((tracks) => {
                dispatch(searchRequest(tracks));
            })
            .catch((err) => {
                console.error(err);
            })
    }
}
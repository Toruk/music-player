
import { CLIENT_ID, REDIRECT_URI } from '../constants/config';
import * as actionTypes from '../constants/actionTypes';
import Cookies from 'js-cookie';

const COOKIE_PATH = 'accessToken';

function setMe(user) {
    return {
        type: actionTypes.ME_SET,
        user
    };
}

export function initAuth() {
    return dispatch => {
        const accessToken = Cookies.get(COOKIE_PATH);

        if (accessToken) {

        }else{
            dispatch(authUser());
        }
    };
}

export function logoutPlayer() {
    Cookies.remove(COOKIE_PATH);
    // Will need to remove/reset state
}

function authUser() {
    return function(dispatch) {
        SC.initialize({ client_id: CLIENT_ID, redirect_uri: REDIRECT_URI });

        SC.connect().then((session) => {
            fetch(`//api.soundcloud.com/me?oauth_token=${session.oauth_token}`)
                .then((response) => response.json())
                .then((me) => {
                    Cookies.set(COOKIE_PATH, session.oauth_token);
                    dispatch(setMe(me));
                    // dispatch(fetchStream(me, session));
                });
        });
    };
}

function fetchStream(me, session) {
    return function(dispatch) {
        fetch(`//api.soundcloud.com/me/activities?limit=20&offset=0&oauth_token=${session.oauth_token}`)
            .then((response) => response.json())
            .then((data) => {
                // dispatch(setTracks(data.collection));
            })
    }
}
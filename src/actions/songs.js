import Axios from 'axios';

import * as actionTypes from '../constants/actionTypes';
import { constructSongDataUrl } from '../utils/songUtils';

// Temp music data - for offline use ONLY
import offlineMusicData from '../../assets/data/music.json';

/**
 * Fetch song
 */
function fetchSongRequest(data) {
    return {
        type: actionTypes.SONGS_FETCH_SONG,
        data
    };
}

export function fetchSCSong(songId) {
    return (dispatch) => {
        Axios.get(constructSongDataUrl(songId))
            .then((res) => {
                dispatch(fetchSongRequest(res.data));
            })
            .catch((err) => console.log(err));
    }
}

export function fetchSong(trackID) {
    let data = offlineMusicData.music;
    for (let i=0; i<data.length; i++) {
        if (trackID === data[i].id) {
            return (dispatch) => {
                dispatch(fetchSongRequest(data[i]));
            }
        }
    }
}



// export function fetchSong(songId) {
//     // Request song
//     Axios.get(constructSongUrl(songId))
//         .then(function(response) {
//             fetchSongRequest(response.data.id)
//         })
//         .catch(error => console.log(error));
// }
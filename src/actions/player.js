import * as actionTypes from '../constants/actionTypes';

export function getPlayerHeight(height) {
    return {
        type: actionTypes.GET_PLAYER_HEIGHT,
        height
    }
}

export function changeCurrentTime(elapsed) {
    return {
        type: actionTypes.CHANGE_CURRENT_TIME,
        elapsed
    }
}

export function changeProgressPos(pos) {
    return {
        type: actionTypes.CHANGE_PROGRESS_POS,
        pos
    }
}

export function playStatus(state) {

        return {
            type: actionTypes.CHANGE_PLAYER_STATE,
            state
        }

}

export function setDuration(duration) {
    return {
        type: actionTypes.SET_SONG_DURATION,
        duration
    }
}
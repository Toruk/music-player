import Axios from 'axios';
import * as actionTypes from '../constants/actionTypes';


function requestSonosState(res) {
    return {
        type: actionTypes.SONOS_SPEAKER_STATE,
        state: res.data
    }
}

export function getSonosState() {
    return (dispatch) => {
        Axios.get('http://localhost:5005/Table of Doom/state')
            .then((res) => {
                dispatch(requestSonosState(res));
            })
            .catch((err) => {
                console.error(err);
            })
    }
}

function requestPauseMusic(pause) {
    return {
        type: actionTypes.SONOS_SPEAKER_PAUSE,
        pause
    };
}

export function getSonosPause() {
    return (dispatch) => {
        Axios.get('http://localhost:5005/Table of Doom/pause')
            .then(function (res) {
                dispatch(requestPauseMusic(res));
                console.log('Successfully Stopped sonos')
            })
            .catch(function (err) {
                console.error(err)
            });
    }
}
function requestPlayMusic(play) {
    return {
        type: actionTypes.SONOS_SPEAKER_PAUSE,
        play
    };
}

export function getSonosPlay() {
    return (dispatch) => {
        Axios.get('http://localhost:5005/Table of Doom/play')
            .then(function (res) {
                dispatch(requestPlayMusic(res));
                console.log('Successfully Playing sonos')
            })
            .catch(function (err) {
                console.error(err)
            });
    }
}
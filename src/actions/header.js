import * as actionTypes from '../constants/actionTypes';

export function getHeaderHeight(height) {
    return {
        type: actionTypes.GET_HEADER_HEIGHT,
        height
    }
}
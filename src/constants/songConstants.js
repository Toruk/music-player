import {CLIENT_ID} from './config';

export const urlClientId    = `?client_id=${CLIENT_ID}`;
export const trackUrl       = 'http://api.soundcloud.com/tracks/';
export const trackQueryUrl  = `http://api.soundcloud.com/tracks${urlClientId}&q=`;
export const trackGenreUrl  = `http://api.soundcloud.com/tracks${urlClientId}&genre=`;

export const ME_SET                         = 'ME_SET';
export const TRACKS_SET                     = 'TRACKS_SET';
export const TRACK_PLAY                     = 'TRACK_PLAY';
export const SET_PLAYER_STATUS              = 'SET_PLAYER_STATUS';

// Header
export const GET_HEADER_HEIGHT              = 'GET_HEADER_HEIGHT';

// Player
export const CHANGE_CURRENT_TIME            = 'CHANGE_CURRENT_TIME';
export const CHANGE_PROGRESS_POS            = 'CHANGE_PROGRESS_POS';
export const CHANGE_PLAYER_STATE            = 'CHANGE_PLAYER_STATE';
export const CHANGE_PLAYING_SONG            = 'CHANGE_PLAYING_SONG';
export const CHANGE_SELECTED_PLAYLISTS      = 'CHANGE_SELECTED_PLAYLISTS';
export const SET_SONG_DURATION              = 'SET_SONG_DURATION';
export const GET_PLAYER_HEIGHT              = 'GET_PLAYER_HEIGHT';

// Songs
export const SONGS_FETCH_SONG               = 'SONGS_FETCH_SONG';

// Sonos
export const SONOS_SPEAKER_STATE            = 'SONOS_SPEAKER_STATE';
export const SONOS_SPEAKER_PLAY             = 'SONOS_SPEAKER_PLAY';
export const SONOS_SPEAKER_PAUSE            = 'SONOS_SPEAKER_PAUSE';

export const SEARCH_QUERY                   = 'SEARCH_QUERY';
import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';

// Import components
import Player from './../components/Player/index';
import Header from './../components/Header/index';
import Sidebar from '../components/Guide Menu/index';
import Panel from '../components/Panel/index';
// import SonosControls from '../components/Sonos/index';

import {getSonosState} from './../actions/sonos';


class AppContainer extends Component {

    componentDidMount() {
        const {dispatch} = this.props;
        dispatch(getSonosState);
    }

    render() {
        const { dispatch } = this.props;
        const { player, header, songs, search, sonos } = this.props.state;

        return (
            <div>
                <Header
                    dispatch={dispatch}
                />
                <Player
                    dispatch={dispatch}
                    player={player}
                    songs={songs}
                />
                <Sidebar offset={header.height + player.height} dispatch={dispatch} songs={songs} player={player} />
                <Panel offset={header.height + player.height} dispatch={dispatch} result={search} />
                {/*<SonosControls dispatch={dispatch} sonos={sonos.state} />*/}
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {state};
}

export default connect(mapStateToProps)(AppContainer);
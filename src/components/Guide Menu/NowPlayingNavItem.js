import React, {Component} from 'react';

import { formatSeconds } from './../../utils/songUtils';

class NowPlayingNavItem extends Component {
    
    render() {
        const {trackTitle, trackDuration, trackElapsed, trackArtwork} = this.props;
        return (
            <li className="guide-menu__section">
                <div className="guide-menu__header">
                    <h3 className="guide-menu__heading">Now Playing</h3>
                </div>
                <div className="now-playing">
                    <div className="now-playing__container">
                        <h4 className="now-playing__track-title">{trackTitle}</h4>
                        <div className="now-playing__track-details">
                            <span className="now-playing__elapsed">{formatSeconds(trackElapsed)}</span> | <span className="now-playing__duration">{formatSeconds(trackDuration)}</span>
                        </div>
                    </div>
                </div>
            </li>
        );
    }
}

export default NowPlayingNavItem;
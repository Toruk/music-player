import React, {Component} from 'react';

// Import sub components / sub menu items
import NowPlayingNavItem from './NowPlayingNavItem';
import PlaylistNavItem from './PlaylistNavItem';
import GenreNavItem from './GenreNavItem';
import LibraryNavItem from './LibraryNavItem';

class GuideMenu extends Component {
    render() {
        const {dispatch, player, songs} = this.props;
        return (
            <div className="guide-menu" style={{top: this.props.offset, minHeight: document.documentElement.clientHeight - this.props.offset}}>
                <div className="guide-menu__container">
                    <ul className="guide-menu__top-level">
                        <NowPlayingNavItem trackTitle={songs.title} trackDuration={player.duration} trackElapsed={player.elapsed} />
                        
                        <GenreNavItem dispatch={dispatch} />
                        <PlaylistNavItem />
                    </ul>


                </div>
            </div>
        );
    }
}

export default GuideMenu;
import React, {Component} from 'react';

import {searchGenre} from './../../actions/search';

class GenreNavItem extends Component {
    constructor(props) {
        super(props);
        this.state = {
            genres: ['pop', 'rock', 'house', 'classical', 'hiphop', 'dubstep', 'garage', 'jazz'],
            genreAccordion: false
        }
    }

    handleGenre(genre, e) {
        const {dispatch} = this.props;
        e.preventDefault();
        // Create an action to dispatch a soundcloud search using the genre as the query
        dispatch(searchGenre(`genre=${genre}`));
    }

    renderItems() {
        let genres = this.state.genres;
        let items = [];
        for (let i=0; i < genres.length; i++) {
            items.push(<li key={i}><a href="#" onClick={this.handleGenre.bind(this, this.state.genres[i])} className="guide-menu__link">{this.state.genres[i]}</a></li>);
        }
        return (
            <ul className="guide-menu__links">
                {items}
            </ul>
        );
    }

    handleToggle() {
        this.setState({genreAccordion: !this.state.genreAccordion});
    }

    render() {
        return (
            <li className="guide-menu__section">
                <div className="guide-menu__header">
                    <h3 className="guide-menu__heading">Genre</h3>
                    <span className="guide-menu__toggleBtn" onClick={this.handleToggle.bind(this)}><i className={'fa ' + (this.state.genreAccordion ? 'fa-minus' : 'fa-plus')}></i></span>
                </div>
                <div className={'guide-menu__accordion ' + (this.state.genreAccordion ? 'active': '')}>
                    {this.renderItems()}
                </div>
            </li>
        );
    }
}

export default GenreNavItem;
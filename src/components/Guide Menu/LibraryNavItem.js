import React, {Component} from 'react';

class LibraryNavItem extends Component {

    render() {
        return (
            <li className="guide-menu__section">
                <div className="guide-menu__header">
                    <h3 className="guide-menu__heading">Library</h3>
                </div>
                <ul className="guide-menu__links">
                    <li>
                        <a href="#" className="guide-menu__link">Your Music</a>
                    </li>
                    <li>
                        <a href="#" className="guide-menu__link">Your Playlists</a>
                    </li>
                </ul>
            </li>
        );
    }
}

export default LibraryNavItem;
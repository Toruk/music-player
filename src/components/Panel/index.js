import React, {Component} from 'react';
import { fetchSCSong, fetchSong } from './../../actions/songs';

class Panel extends Component {

    handleTrackSelect(id) {
        const {dispatch} = this.props;
        dispatch(fetchSCSong(id));
    }

    renderResults() {
        const {result} = this.props;
        if (typeof result !== "undefined") {
            return (
                <div>
                    {result.tracks.map(function(track, i) {
                        return (
                            <div key={i} className="result" onClick={this.handleTrackSelect.bind(this, track.id)}>
                                <div className="result__artwork" style={{backgroundImage: 'url(' + track.artwork_url + ')', backgroundRepeat: 'no-repeat', backgroundSize: 'cover'}}></div>
                                <div className="result__info">
                                    {track.title}
                                </div>
                            </div>


                        );
                    }.bind(this))}
                </div>
            );
        }
    }

    render() {
        return (
            <div className="panel" style={{top: this.props.offset, minHeight: document.documentElement.clientHeight - this.props.offset}}>
                {this.renderResults()}
            </div>
        );
    }
}

export default Panel;
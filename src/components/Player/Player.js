import React, {Component} from 'react';

// Sub Components
import Sound from './Sound';
import Controls from './Controls';
import Visualiser from './Visualiser';

// Actions
import { getPlayerHeight } from './../../actions/player';
import { fetchSCSong, fetchSong } from './../../actions/songs';
import { formatSeconds } from './../../utils/songUtils';

class Player extends Component {
    constructor(props) {
        super(props);

        this.state = {
            currentSongId: null
        }
    }
    // Temp
    componentDidMount() {
        const { dispatch } = this.props;

        // Set the player height and dispatch action
        const playerHeight = document.querySelector('.player').clientHeight;
        dispatch(getPlayerHeight(playerHeight));

        dispatch(fetchSCSong(126522548));
        // Fetch offline song from the assets folder - TODO refactor json response
        // dispatch(fetchSong(1));

        this.setState({
            currentSongId: 126522548
        });
    }

    render() {
        const { dispatch, player, songs } = this.props;
        return (
            <div className="player">
                <Sound isPlaying={player.playStatus} progressPos={player.progressPos} currentPos={player.elapsed} url={songs.stream_url} dispatch={dispatch} />
                <div className="flex-container">
                    <Visualiser songUrl={songs.stream_url} currentPos={player.elapsed} duration={player.duration} dispatch={dispatch} />
                </div>
                <Controls isPlaying={player.playStatus} elapsed={player.elapsed} duration={player.duration} dispatch={dispatch} />
            </div>
        );
    }
}

export default Player;
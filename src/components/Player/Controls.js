import React, {Component} from 'react';
import Progress from './Progress';

// Import Actions
import { playStatus } from './../../actions/player';

class Controls extends Component {

    handleTogglePlay() {
        const { dispatch, isPlaying } = this.props;
        if (isPlaying) {
            // Pause
            dispatch(playStatus(false));
        }else{
            // Play
            dispatch(playStatus(true));
        }
    }

    render() {
        const { isPlaying, elapsed, duration, dispatch } = this.props;
        return (
            <div className="controls">
                <div className="controls__inner">
                    <div className="controls__buttons">
                        <a href="#" className="btn btn--controls btn--play-pause" onClick={this.handleTogglePlay.bind(this)}><i className={'fa ' + (isPlaying ? 'fa-pause' : 'fa-play')}></i></a>
                    </div>
                    <Progress elapsed={elapsed} duration={duration} dispatch={dispatch} />
                </div>
            </div>
        );
    }
}

export default Controls;
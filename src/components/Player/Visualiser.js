import React, {Component} from 'react';
import Wavesurfer from 'react-wavesurfer';

class Visualiser extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isLoading: false
        }
    }

    handleLoading() {
        this.setState({isLoading: true});
    }

    handleReady() {
        this.setState({isLoading: false});
    }

    render() {
        const { songUrl, currentPos } = this.props;

        return (
            <div className="visualiser">
                <div className="visualiser__wave">
                    <Wavesurfer
                        audioFile={songUrl}
                        pos={currentPos}
                        onLoading={this.handleLoading.bind(this)}
                        onReady={this.handleReady.bind(this)}
                        options={{
                            barWidth: 1,
                            height: 60,
                            progressColor: 'rgba(46,204,113, 0.8)',
                            waveColor: 'rgba(41,128,185, 0.1)',
                            cursorColor: 'rgba(255,255,255,0.1)',
                            cursorWidth: 1,
                            interact: false,
                            setPlaybackRate: 1
                        }}
                    />
                </div>
                <div className={'visualiser__loader ' + (this.state.isLoading ? '' : 'hide')}>
                    <div className="loader"></div>
                </div>
            </div>
        );
    }
}

export default Visualiser;
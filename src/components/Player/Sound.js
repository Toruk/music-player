import React, {Component} from 'react';
import ReactDOM from 'react-dom';

import { changeCurrentTime, changeProgressPos, setDuration, playStatus} from './../../actions/player';

class Sound extends Component {
    constructor(props) {
        super(props);
        this.audioElement = undefined;
    }

    componentDidMount() {
        this.audioElement = ReactDOM.findDOMNode(this.refs.audio);
        this.audioElement.addEventListener('ended', this.handleEnded.bind(this), false);
        this.audioElement.addEventListener('loadstart', this.handleLoadStart.bind(this), false);
        this.audioElement.addEventListener('loadedmetadata', this.handleLoadedMetadata.bind(this), false);
        this.audioElement.addEventListener('timeupdate', this.handleTimeUpdate.bind(this), false);
        this.audioElement.pause();
    }
    componentDidUpdate() {
        const {dispatch, currentPos, progressPos, isPlaying} = this.props;
        if (isPlaying) {
            this.audioElement.play();
        }else{
            this.audioElement.pause();
        }
        if (progressPos !== null) {
            this.audioElement.currentTime = progressPos;
            dispatch(changeCurrentTime(currentPos));
            dispatch(changeProgressPos(null));
        }
    }

    handleEnded() {
        const {dispatch} = this.props;
        this.audioElement.pause();
        dispatch(changeCurrentTime(0));
        dispatch(playStatus(false));

    }

    handleLoadStart() {
        const { dispatch } = this.props;
        // change song position to 0
        dispatch(changeCurrentTime(0));
    }

    handleLoadedMetadata(e) {
        const { dispatch } = this.props;
        dispatch(setDuration(this.audioElement.duration));
    }

    handleTimeUpdate(e) {
        const { dispatch } = this.props;
        let audioCurrentTime = Math.floor(this.audioElement.currentTime);
        dispatch(changeCurrentTime(audioCurrentTime));
    }

    render() {
        return <audio id='audio' ref='audio' src={this.props.url}></audio>;
    }
}

export default Sound;
import React, {Component} from 'react';

import { changeCurrentTime, changeProgressPos } from '../../actions/player';

class Progress extends Component {
    handleSeek(e) {
        const { elapsed, duration, dispatch } = this.props;
        let currentTime = Math.floor((e.nativeEvent.layerX / e.currentTarget.offsetWidth) * duration);
        e.persist();
        dispatch(changeCurrentTime(currentTime));
        dispatch(changeProgressPos(currentTime));
    }

    render() {
        const {elapsed, duration} = this.props;
        let width = (elapsed/duration) * 100;
        return (
            <div className="progress">
                <div className="progress__bar" onClick={this.handleSeek.bind(this)}>
                    <div className="progress__inner">
                        <div className="progress__elapsed" style={{width: `${width}%`}}></div>
                        <div className="progress__pointer" style={{left: `${width}%`}}></div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Progress;
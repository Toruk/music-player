import React, {Component} from 'react';

import Image from './../Generic/Image';

class Details extends Component {

    render() {
        let artwork = this.props.artwork;
        let title = this.props.title;
        let duration = this.props.duration;
        let elapsed = this.props.elapsed;

        return (
            <div className="details">
                <Image imgClass="details__artwork" imgSrc={artwork} inline="true" />
                <div className="details__info">
                    <h3 className="no-spacing">{title}</h3>
                    <p className="no-spacing">{elapsed} | {duration}</p>
                </div>
            </div>
        );
    };
}

export default Details;
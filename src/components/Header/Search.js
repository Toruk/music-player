import React, { Component } from 'react';

// Actions
import { searchQuery } from './../../actions/search';

class Search extends Component {
    constructor(props) {
        super(props);

        this.state = {
            query: null
        }
    }

    handleInputChange(e) {
        let value = e.target.value;
        this.setState({query: value});
    }

    handleSearch(e) {
        e.preventDefault();
        const { dispatch } = this.props;
        // Dispatch search query // Searches Soundcloud api for songs relating to the search query
        dispatch(searchQuery(this.state.query));
    }

    render() {
        return (
            <div className="header__search">
                <form action="">
                    <input type="text" placeholder="Search..." className="input" onChange={this.handleInputChange.bind(this)} />
                    <button type="submit" onClick={this.handleSearch.bind(this)}><i className="fa fa-search"></i></button>
                </form>
            </div>
        );
    }
}

export default Search;
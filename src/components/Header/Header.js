import React, { Component } from 'react';

import Search from './Search';

import { getHeaderHeight } from './../../actions/header';


class Header extends Component {

    componentDidMount() {
        const {dispatch} = this.props;

        // Set the player height and dispatch action
        const headerHeight = document.querySelector('.header').clientHeight;
        dispatch(getHeaderHeight(headerHeight));
    }

    render() {
        const { dispatch } = this.props;
        return (
            <header className="header">
                <div className="header__inner">
                    <div className="header__left">
                        <div className="header__logo">
                        {/* Player Logo */}
                        </div>
                    </div>
                    <div className="header__middle"></div>
                    <div className="header__right">
                        <Search dispatch={dispatch}/>
                    </div>
                </div>

            </header>
        );
    }
}

export default Header;
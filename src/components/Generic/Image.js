import React, {Component} from 'react';

class Image extends Component {

    render() {
        const inline      = this.props.inline;
        const imgClass    = this.props.imgClass;
        const imgSrc      = this.props.imgSrc;
        const imgAlt      = this.props.imgAlt;

        if (inline) {
            return <div className={imgClass} style={{background: 'url(' + imgSrc + ') top left / cover no-repeat'}}></div>
        }else{
            return <img src={imgSrc} alt={imgAlt} className={imgClass} />;
        }
    }
}

export default Image;
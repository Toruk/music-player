import React, { Component } from 'react';
import {getSonosState, getSonosPause, getSonosPlay} from './../../actions/sonos';

class Sonos extends Component {
    constructor(props) {
        super(props);
    }
    componentDidMount() {
        let {dispatch} = this.props;
        dispatch(getSonosState());

        setInterval(() => {
            dispatch(getSonosState());
        }, 10000);
    }

    handleTogglePlay() {
        const {dispatch} = this.props;
        dispatch(getSonosPlay());
    }
    handleToggleStop() {
        const {dispatch} = this.props;
        dispatch(getSonosPause());
    }

    render() {
        const {dispatch, sonos} = this.props;

        if (sonos !== null) {
            return(
                <div className="sonos">
                    <div className="sonos__controls">
                        <a href="#" className="btn btn--controls" onClick={this.handleTogglePlay.bind(this)}><i className={'fa ' + (sonos.playerState == 'PLAYING' ? 'fa-pause' : 'fa-play')}></i></a>

                        <a href="#" className="btn btn--controls" onClick={this.handleToggleStop.bind(this)}><i className="fa fa-volume-off"></i></a>
                    </div>
                    <div className="sonos__info">
                        <p>{sonos.currentTrack.title}</p>
                        {/*<p>{sonos.currentTrack.artist}</p>*/}
                    </div>
                </div>
            );
        }else{
            return <div></div>;
        }
    }
}

export default Sonos;
import { combineReducers } from 'redux';
// import auth from './auth';

import header from '../reducers/header';
import player from '../reducers/player';
import songs from '../reducers/songs';
import sonos from '../reducers/sonos';
import search from '../reducers/search';

export default combineReducers({
    header,
    player,
    songs,
    search,
    sonos
});
import * as actionTypes from '../constants/actionTypes';

const initialState = {
    tracks: []
};

export default function(state = initialState, action) {
    switch(action.type) {
        case actionTypes.SEARCH_QUERY:
            return Object.assign({}, state, {
                tracks: action.query
            });
        default:
            return state;
    }
}
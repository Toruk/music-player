import * as actionTypes from '../constants/actionTypes';

const initialState = {
    height: 0, // 0 because initially the height will be unknown
    currentSongIndex: null,
    currentTime: 0,
    duration: 0,
    progressPos: null,
    playing: false,
    selectedPlaylists: []
};

export default function(state = initialState, action) {
    switch(action.type) {
        case actionTypes.CHANGE_CURRENT_TIME:
            return Object.assign({}, state, {
                elapsed: action.elapsed
            });
        case actionTypes.CHANGE_PROGRESS_POS:
            return Object.assign({}, state, {
                progressPos: action.pos
            });
        case actionTypes.CHANGE_PLAYER_STATE:
            return Object.assign({}, state, {
                playStatus: action.state
            });

        case actionTypes.SET_SONG_DURATION:
            return Object.assign({}, state, {
                duration: action.duration
            });
        case actionTypes.GET_PLAYER_HEIGHT:
            return Object.assign({}, state, {
                height: action.height
            });
        default:
            return state;
    }
}
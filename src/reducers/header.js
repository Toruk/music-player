import * as actionTypes from '../constants/actionTypes';

const initialState = {
    height: 0
};

export default function(state = initialState, action) {
    switch(action.type) {
        case actionTypes.GET_HEADER_HEIGHT:
            return Object.assign({}, state, {
                height: action.height
            });
        default:
            return state;
    }
}
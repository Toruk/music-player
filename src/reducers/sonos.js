import * as actionTypes from '../constants/actionTypes';

const initialState = {
    state: null
};

export default function(state = initialState, action) {
    switch(action.type) {
        case actionTypes.SONOS_SPEAKER_STATE:
            return Object.assign({}, state, {
                state: action.state
            });
        case actionTypes.SONOS_SPEAKER_PLAY:
            return Object.assign({}, state, {
                play: action.play
            });
        case actionTypes.SONOS_SPEAKER_PAUSE:
            return Object.assign({}, state, {
                pause: action.pause
            });
        default:
            return state;
    }
}
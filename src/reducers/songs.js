import * as actionTypes from '../constants/actionTypes';

import { constructSongUrl } from './../utils/songUtils';

const initialState = {
    id: null,
    user_id: null,
    stream_url: 'https://api.soundcloud.com/tracks/272391865/stream?client_id=11c11646280749c019f8e57a4a1f520c',
    title: '',
    description: '',
    artwork_url: ''
};

export default function(state = initialState, action) {

    switch(action.type) {
        case actionTypes.SONGS_FETCH_SONG:
            return Object.assign({}, state, {
                id: action.data.id,
                stream_url: constructSongUrl(action.data.stream_url),
                title: action.data.title,
                artwork_url: action.data.artwork_url,
                description: action.data.description
            });
        default:
            return state;
    }
}
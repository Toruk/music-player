import { urlClientId, trackUrl } from '../constants/songConstants';

export function constructSongDataUrl(songId) {
    return `${trackUrl}${songId}${urlClientId}`;
}

export function constructSongUrl(url) {
    return `${url}${urlClientId}`;
}

export function formatMilliseconds(milliseconds) {
        // Format hours
        let hours = Math.floor(milliseconds / 3600000);
        milliseconds = milliseconds % 3600000;

        // Format minutes
        let minutes = Math.floor(milliseconds / 60000);
        milliseconds = milliseconds % 60000;

        // Format seconds
        let seconds = Math.floor(milliseconds / 1000);
        milliseconds = Math.floor(milliseconds % 1000);
        // return as string
        return (minutes < 10 ? '0' : '') + minutes + ':' + (seconds < 10 ? '0' : '') + seconds;
}

export function formatSeconds(s) {
    // Format Minutes
    let minutes = Math.floor(s / 60);
    s = s % 60;
    let seconds = Math.floor(s / 1);
    s = Math.floor(s % 1);
    return (minutes < 10 ? '0' : '') + minutes + ':' + (seconds < 10 ? '0' : '') + seconds;
}
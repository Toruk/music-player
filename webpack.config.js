var Webpack = require('webpack');
var ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
    entry: [
        'webpack-dev-server/client?http://0.0.0.0:8080',
        'webpack/hot/only-dev-server',
        './src/index.js',
        './src/scss/main.scss'
    ],
    output: {
        path: __dirname,
        publicPath: 'http://0.0.0.0:8080/',
        filename: '/public/bundle.js'
    },
    module: {
        loaders: [
            {
                test: /\.js$/,
                loaders: ['react-hot', 'jsx', 'babel'],
                exclude: /node_modules/
            },
            {
                test: /\.scss$/,
                loader: ExtractTextPlugin.extract('css!sass')
            },
            {
                test: /\.(png|jpg|jpeg|gif|woff|eot|tff|svg|eot@|mp4)$/,
                loader: 'url-loader?limit=8192'
            },
            {
                test: /\.(json)/,
                loader: 'json-loader'
            }
        ]
    },
    resolve: {
        extensions: ['', '.js', '.jsx']
    },
    devServer: {
        historyApiFallback: true,
        contentBase: './',
        host: '0.0.0.0',
        port: 8080,
    },
    plugins: [
        new Webpack.HotModuleReplacementPlugin(),
        new ExtractTextPlugin('public/style.css', {
            allChunks: true
        }),
        new Webpack.ProvidePlugin({
            'fetch': 'imports?this=>global!exports?global.fetch!whatwg-fetch'
        })
    ]
};